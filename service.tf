
resource "kubernetes_deployment" "service" {
  metadata {
    name = var.service_name
    labels = {
      app = var.service_name
    }
  }
  spec {
    selector {
      match_labels = {
        app = var.service_name
      }
    }

    template {
      metadata {
        labels = {
          app = var.service_name
        }
      }
      spec {
        container {
          image = "jodogne/orthanc"
          name = var.service_name
          port {
            container_port =var.container_port1
          }
          port {
            container_port =  var.container_port2
          }

        }
      }
    }
  }
}



resource "kubernetes_service" "dicom-service" {
  depends_on = [kubernetes_deployment.service]
  metadata {
    name      = "${var.service_name}-service"
  }
  spec {
    selector = {
      app = var.service_name
    }
    type = "NodePort"
    port {
      name="nodeport1"
      node_port   = var.node_port1
      protocol = "TCP"
      port = var.container_port1
      target_port = var.container_port1
    }
    port {
      name = "nodeport2"
      node_port   =  var.node_port2
      protocol = "TCP"
      port = var.container_port2
      target_port = var.container_port2
    }
  }
}

