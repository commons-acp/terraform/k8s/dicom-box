variable "container_port1" {
  default = "4242"
  description = " DICOM service on Port 4242 "
}
variable "container_port2" {
  default = "8042"
  description = " the web interface on port 8042 "

}
variable "service_name" {
  description = ""
}
variable "node_port1" {
  description = "export dicom service port  on node_port1"
}
variable "node_port2" {
  description = "export  the web interface  on node_port2"
}
